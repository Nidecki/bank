package pl.nidecki.bank.domain;

import lombok.SneakyThrows;
import pl.nidecki.bank.config.Config;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.LocalTime;

import static java.lang.String.format;
import static org.apache.commons.lang3.ObjectUtils.notEqual;

record LoanApplicationValidator(Config config, Clock clock) {

    void checkApplication(LoanApplication application) {
        checkTerm(application.term());
        checkAmount(application.amount());
        checkIfMaxAmountInExcludedTime(application.amount());
    }

    @SneakyThrows
    void checkTerm(Integer term) {
        var isInRange = term >= config.getMinTerm()
                && term <= config.getMaxTerm();
        if (isInRange) {
            return;
        }
        throw new ValidationException(format("The requested term is not within required range- between %s and %s",
                config.getMinTerm(),
                config.getMaxTerm()));
    }

    @SneakyThrows
    private void checkAmount(BigDecimal amount) {
        var isInRange = amount.compareTo(config.getMinAmount()) >= 0
                && amount.compareTo(config.getMaxAmount()) <= 0;
        if (isInRange) {
            return;
        }
        throw new ValidationException(format("The requested amount is not within required range- between %s and %s",
                config.getMinAmount(),
                config.getMaxAmount()));
    }

    @SneakyThrows
    private void checkIfMaxAmountInExcludedTime(BigDecimal amount) {
        if (notEqual(amount,
                config.getMaxAmount())) {
            return;
        }
        var applicationTime = LocalTime.now(clock);
        var inExcludedTime = applicationTime.isAfter(config.getRejectionStartTime()) &&
                applicationTime.isBefore(config.getRejectionEndTime());
        if (inExcludedTime) {
            throw new ValidationException(format("Maximum amount cannot be requested between %s and %s",
                    config.getRejectionStartTime(),
                    config.getRejectionEndTime()));
        }
    }
}
