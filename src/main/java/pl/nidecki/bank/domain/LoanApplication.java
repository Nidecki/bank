package pl.nidecki.bank.domain;


import java.math.BigDecimal;

public record LoanApplication(BigDecimal amount, Integer term) {
}
