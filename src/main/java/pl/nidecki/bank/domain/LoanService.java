package pl.nidecki.bank.domain;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.nidecki.bank.config.Config;
import pl.nidecki.bank.port.LoanRepository;

import java.time.Clock;
import java.time.LocalDate;


@Slf4j
@Service
public class LoanService {
    private final Clock clock;
    private final LoanApplicationValidator validator;
    private final LoanRepository loanRepository;
    private final Config config;

    LoanService(Clock clock, Config config, LoanRepository loanRepository) {
        this.clock = clock;
        this.config = config;
        this.loanRepository = loanRepository;
        this.validator = new LoanApplicationValidator(config, clock);
    }

    public Loan issueLoan(LoanApplication application) {
        log.info("Got loan application: {}", application);
        validator.checkApplication(application);
        var loan = Loan.builder()
                .amount(application.amount())
                .term(application.term())
                .dueDate(LocalDate.now(clock).plusDays(application.term()))
                .principal(config.getLoanPrincipal())
                .build();

        loanRepository.saveOrUpdate(loan);
        log.info("Loan issued successfully: {}", loan);
        return loan;
    }

    public Loan extendLoan(String loanId) {
        log.info("Got extension application for loan of id {}", loanId);
        var loan = fetch(loanId);
        validator.checkTerm(loan.getTerm() + config.getExtensionTerm());
        var extendedLoan = loan.extended(config.getExtensionTerm());
        loanRepository.saveOrUpdate(extendedLoan);
        log.info("Loan extended successfully: {}", extendedLoan);
        return extendedLoan;
    }

    public Loan fetch(String loanId) {
        return loanRepository.findById(loanId);
    }
}
