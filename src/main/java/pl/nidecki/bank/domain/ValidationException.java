package pl.nidecki.bank.domain;

public class ValidationException extends Exception {
    public ValidationException(String message) {
        super(message);
    }
}
