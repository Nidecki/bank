package pl.nidecki.bank.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

import static lombok.AccessLevel.PRIVATE;


@Builder
@Value
@AllArgsConstructor(access = PRIVATE)
public class Loan {
    @Builder.Default
    String loanId = UUID.randomUUID().toString().replace("-", "");
    BigDecimal amount;
    Integer term;
    LocalDate dueDate;
    BigDecimal principal;

    Loan extended(Integer additionalTerm) {
        return Loan.builder()
                .loanId(loanId)
                .amount(amount)
                .term(term + additionalTerm)
                .dueDate(dueDate.plusDays(additionalTerm))
                .principal(principal)
                .build();
    }
}

