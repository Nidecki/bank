package pl.nidecki.bank.application;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.nidecki.bank.domain.Loan;
import pl.nidecki.bank.domain.LoanApplication;
import pl.nidecki.bank.domain.LoanService;
import pl.nidecki.bank.domain.ValidationException;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestController
@RequiredArgsConstructor
@RequestMapping("loans")
public class LoanController {
    private final LoanService loanService;

    @PostMapping
    Loan issue(@RequestBody LoanApplication application) {
        return loanService.issueLoan(application);
    }

    @PutMapping("/{loanId}")
    Loan extend(@PathVariable String loanId) {
        return loanService.extendLoan(loanId);
    }

    @GetMapping("/{loanId}")
    Loan fetch(@PathVariable String loanId) {
        return loanService.fetch(loanId);
    }

    @ExceptionHandler(value = ValidationException.class)
    public ResponseEntity<Object> exception(ValidationException exception) {
        return new ResponseEntity<>(exception.getMessage(), BAD_REQUEST);
    }
}
