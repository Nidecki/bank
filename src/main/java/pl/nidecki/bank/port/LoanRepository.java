package pl.nidecki.bank.port;

import pl.nidecki.bank.domain.Loan;

public interface LoanRepository {
    void saveOrUpdate(Loan Loan);

    Loan findById(String loanId);
}
