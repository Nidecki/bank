package pl.nidecki.bank.adapter;

import org.springframework.stereotype.Repository;
import pl.nidecki.bank.domain.Loan;
import pl.nidecki.bank.port.LoanRepository;

import java.util.HashMap;


@Repository
class LoanInMemoryRepository implements LoanRepository {
    private final HashMap<String, Loan> loanMap = new HashMap<>();

    @Override
    public void saveOrUpdate(Loan loan) {
        loanMap.put(loan.getLoanId(), loan);
    }

    @Override
    public Loan findById(String loanId) {
        return loanMap.get(loanId);
    }
}
