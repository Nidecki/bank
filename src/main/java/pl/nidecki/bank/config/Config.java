package pl.nidecki.bank.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.validation.annotation.Validated;

import javax.annotation.PostConstruct;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalTime;


@Slf4j
@Getter
@ToString
@Validated
@AllArgsConstructor
@ConstructorBinding
@ConfigurationProperties(prefix = "app.configuration")
public class Config {
    @Min(1)
    @NotNull
    @Digits(integer = 20, fraction = 2)
    private BigDecimal maxAmount;

    @Min(1)
    @NotNull
    @Digits(integer = 20, fraction = 2)
    private BigDecimal minAmount;

    @Min(1)
    @NotNull
    private Integer minTerm;

    @Min(1)
    @NotNull
    private Integer maxTerm;

    @NotNull
    private LocalTime rejectionStartTime;

    @NotNull
    private LocalTime rejectionEndTime;

    @Min(1)
    @NotNull
    @Digits(integer = 20, fraction = 2)
    private Integer extensionTerm;

    @NotNull
    @DecimalMin(value = "0.0", inclusive = false)
    private BigDecimal loanPrincipal;

    @PostConstruct
    void setUp() {
        if (extensionTerm >= maxTerm) {
            throw new IllegalArgumentException("Default extension term cannot exceed max term.");
        }
        if (rejectionStartTime.isAfter(rejectionEndTime)) {
            throw new IllegalArgumentException("Rejection start time must be before end time.");
        }
        log.info("App started with the configuration: {}", this);
    }
}
