package pl.nidecki.bank;

import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class BankApplicationIntegrationTest {
    @Autowired
    private MockMvc mvc;

    @Test
    void issueAndExtendSuccessfully() throws Exception {
        var issueResponse = mvc.perform(post("/loans")
                        .content("""
                                {
                                    "term": "12",
                                    "amount": 133.00
                                }""")
                        .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        var loanId = JsonPath.read(issueResponse, "$.loanId");

        var extendResponse = mvc.perform(put("/loans/" + loanId)
                        .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        var extendedTerm = JsonPath.read(extendResponse, "$.term");

        assertThat(extendedTerm).isEqualTo(26);
    }

}
