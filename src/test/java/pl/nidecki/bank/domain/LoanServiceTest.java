package pl.nidecki.bank.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.nidecki.bank.config.Config;
import pl.nidecki.bank.port.LoanRepository;

import java.time.*;

import static java.math.BigDecimal.valueOf;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LoanServiceTest {
    @Mock
    private Clock clock;
    @Mock
    private Config config;
    @Mock
    private LoanRepository loanRepository;
    @InjectMocks
    private LoanService loanService;

    @Test
    void issueSuccessfully() {
        //given
        when(clock.instant()).thenReturn(Instant.parse("2022-04-22T10:15:30Z"));
        when(clock.getZone()).thenReturn(ZoneId.of("UTC"));
        when(config.getLoanPrincipal()).thenReturn(valueOf(12.02));
        mockAmountConstraints();
        mockTermConstraints();

        //when
        var loan = loanService.issueLoan(new LoanApplication(valueOf(15.01), 10));

        //then
        assertAll(() -> verify(loanRepository).saveOrUpdate(loan),
                () -> assertThat(loan.getPrincipal()).isEqualTo(valueOf(12.02)));
    }

    @Test
    void extendSuccessfully() {
        //given
        var loan = Loan.builder()
                .amount(valueOf(15.01))
                .term(10)
                .dueDate(LocalDate.of(2022, 7, 2))
                .principal(valueOf(10.00))
                .build();
        when(loanRepository.findById(loan.getLoanId())).thenReturn(loan);
        mockTermConstraints();
        when(config.getExtensionTerm()).thenReturn(4);

        //when
        var extendedLoan = loanService.extendLoan(loan.getLoanId());

        //then
        assertAll(() -> assertThat(extendedLoan.getDueDate()).isEqualTo("2022-07-06"),
                () -> assertThat(extendedLoan.getTerm()).isEqualTo(14),
                () -> assertThat(extendedLoan.getPrincipal()).isEqualTo(valueOf(10.00)),
                () -> assertThat(extendedLoan.getAmount()).isEqualTo(valueOf(15.01)),
                () -> verify(loanRepository).saveOrUpdate(extendedLoan));
    }

    @Test
    void tooLongExtension() {
        //given
        mockTermConstraints();
        var loan = Loan.builder()
                .amount(valueOf(13.56))
                .term(312)
                .dueDate(LocalDate.of(2022, 9, 11))
                .principal(valueOf(5.00))
                .build();
        when(loanRepository.findById(loan.getLoanId())).thenReturn(loan);
        when(config.getExtensionTerm()).thenReturn(123);

        ///when //then
        var message = assertThrows(ValidationException.class,
                () -> loanService.extendLoan(loan.getLoanId())).getMessage();
        assertThat(message).isEqualTo("The requested term is not within required range- between 2 and 365");
    }

    @Test
    void tooLongTerm() {
        //given
        mockTermConstraints();

        ///when //then
        var message = assertThrows(ValidationException.class,
                () -> loanService.issueLoan(new LoanApplication(valueOf(77), 401))).getMessage();
        assertThat(message).isEqualTo("The requested term is not within required range- between 2 and 365");
    }

    @Test
    void tooShortTerm() {
        //given
        mockTermConstraints();

        var message = assertThrows(ValidationException.class,
                () -> loanService.issueLoan(new LoanApplication(valueOf(77), 1))).getMessage();
        assertThat(message).isEqualTo("The requested term is not within required range- between 2 and 365");
    }

    @Test
    void tooBigAmount() {
        //given
        mockAmountConstraints();
        mockTermConstraints();

        //when //then
        var message = assertThrows(ValidationException.class,
                () -> loanService.issueLoan(new LoanApplication(valueOf(99), 3))).getMessage();
        assertThat(message).isEqualTo("The requested amount is not within required range- between 15 and 88");
    }

    @Test
    void tooSmallAmount() {
        //given
        mockAmountConstraints();
        mockTermConstraints();

        //when //then
        var message = assertThrows(ValidationException.class,
                () -> loanService.issueLoan(new LoanApplication(valueOf(14), 3))).getMessage();
        assertThat(message).isEqualTo("The requested amount is not within required range- between 15 and 88");
    }

    @Test
    void maxAmountAskedDuringExcludedTime() {
        //given
        mockAmountConstraints();
        mockTermConstraints();
        when(clock.instant()).thenReturn(Instant.parse("2022-05-01T02:15:30Z"));
        when(clock.getZone()).thenReturn(ZoneId.of("UTC"));
        when(config.getRejectionStartTime()).thenReturn(LocalTime.of(0, 0));
        when(config.getRejectionEndTime()).thenReturn(LocalTime.of(6, 0));

        var message = assertThrows(ValidationException.class,
                () -> loanService.issueLoan(new LoanApplication(valueOf(88), 3))).getMessage();
        assertThat(message).isEqualTo("Maximum amount cannot be requested between 00:00 and 06:00");

    }

    private void mockAmountConstraints() {
        when(config.getMinAmount()).thenReturn(valueOf(15));
        when(config.getMaxAmount()).thenReturn(valueOf(88));
    }

    private void mockTermConstraints() {
        when(config.getMinTerm()).thenReturn(2);
        when(config.getMaxTerm()).thenReturn(365);
    }
}